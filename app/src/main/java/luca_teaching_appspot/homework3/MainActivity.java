package luca_teaching_appspot.homework3;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.preference.PreferenceManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.location.LocationServices;

import luca_teaching_appspot.chatapp.R;

//See 50min mark lecture 7 for "SharedPreferences" object example
public class MainActivity extends AppCompatActivity implements
        ConnectionCallbacks, OnConnectionFailedListener {

    public final static String LOG_TAG = "MainActivity: ";
    public final static String NICK_ID = "NICK_ID: ";
    public final static String USER_ID = "USER_ID: ";
    public final static String LNG = "Longitude: ";
    public final static String LAT = "Latitude: ";
    public final static String ACC = "Accuracy: ";

    protected GoogleApiClient mGoogleApiClient;
    protected Location mLastLocation;

    private LocationData locationData = LocationData.getLocationData();
    private String userid;
    private Button startChatButton;
    private Button locationButton;
    private TextView locView;
    private int toastResId = 0;
    private int locConstraint = 50; //constrain to 50 meters

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.i(LOG_TAG, "onCreate()");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        locView = (TextView) findViewById(R.id.locView);
        buildGoogleApiClient();
    }

    public void setlocdata(double lat, double lng, double acc){
        String tmp = "Lat: " + lat + "\nLng: " + lng + "\nAccuracy: " + acc;
        locView.setText(tmp);
    }

    @Override
    public void onResume() {
        Log.i(LOG_TAG, "onResume()");
        startChatButton = (Button) findViewById(R.id.startChatButton);
        Boolean locationAllowed = checkLocationAllowed();
        if(locationAllowed){
            mGoogleApiClient.connect();
        } else {
            mGoogleApiClient.disconnect();
            startChatButton.setEnabled(false);
        }
        render();
        super.onResume();
    }

    @Override
    public void onPause(){
        Log.i(LOG_TAG, "onPause()");
        if(checkLocationAllowed())
            //if the user has allowed location sharing we must disable location updates now
            mGoogleApiClient.disconnect();
        super.onPause();
    }

    private void render(){
        //render button text between "Disable Location" and "Enable Location"
        Boolean locationAllowed = checkLocationAllowed();
        locationButton = (Button) findViewById(R.id.locationButton);

        if(locationAllowed) {
            locationButton.setText(R.string.enableLoc);
        }
        else {
            locationButton.setText(R.string.disableLoc);
        }
    }

    private boolean checkLocationAllowed(){
        Log.i(LOG_TAG, "checkLocationAllowed()");
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(this);
        return settings.getBoolean("location_allowed", false);
    }

    private void setLocationAllowed(boolean allowed){
        Log.i(LOG_TAG, "checkLocationAllowed");
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = settings.edit();
        editor.putBoolean("location_allowed", allowed);
        editor.commit();
    }

    private void saveParameters(double lat, double lng, double accuracy){
        Log.i(LOG_TAG, "saveParameters()");
        //setlocdata(lat, lng, accuracy);
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = settings.edit();
        SecureRandomString srs = new SecureRandomString();
        userid = srs.nextString();
        float latfloat = (float) lat;
        float lngfloat = (float) lng;
        float accfloat = (float) accuracy;
        editor.putFloat(LAT, latfloat);
        editor.putFloat(LNG, lngfloat);
        editor.putFloat(ACC, accfloat);
        editor.putString(USER_ID, userid);
        editor.commit();
    }


    public void toggleLocation(View v) {
        Log.i(LOG_TAG, "toggleLocation()");
        Boolean locationAllowed = checkLocationAllowed();

        if(locationAllowed){
            //disable location
            setLocationAllowed(false);
            startChatButton.setEnabled(true);
        } else {
            //enable location
            setLocationAllowed(true);
            startChatButton.setEnabled(false);
        }
        render();
    }


    @Override
    protected void onStop() {
        Log.i(LOG_TAG, "onStop()");
        super.onStop();
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }
///////////////////////Example taken from https://github.com/googlesamples/android-play-location
    /**
     * Runs when a GoogleApiClient object successfully connects.
     */
    @Override
    public void onConnected(Bundle connectionHint) {
        Log.i(LOG_TAG, "onConnected()");
        // Provides a simple way of getting a device's location and is well suited for
        // applications that do not require a fine-grained location and that do not need location
        // updates. Gets the best and most recent location currently available, which may be null
        // in rare cases when a location is not available.
        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (mLastLocation != null) {
            saveParameters(mLastLocation.getLatitude(), mLastLocation.getLongitude(),
                    mLastLocation.getAccuracy());
        } else {
            Toast.makeText(this, R.string.no_location_detected, Toast.LENGTH_LONG).show();
        }
    }

    /**
     * Builds a GoogleApiClient. Uses the addApi() method to request the LocationServices API.
     */
    protected synchronized void buildGoogleApiClient() {
        Log.i(LOG_TAG, "buildGoogleApiClient()");
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        // Refer to the javadoc for ConnectionResult to see what error codes might be returned in
        // onConnectionFailed.
        Log.i(LOG_TAG, "Connection failed: ConnectionResult.getErrorCode() = " + result.getErrorCode());
    }

    @Override
    public void onConnectionSuspended(int cause) {
        // The connection to Google Play services was lost for some reason. We call connect() to
        // attempt to re-establish the connection.
        Log.i(LOG_TAG, "Connection suspended");
        mGoogleApiClient.connect();
    }
//////////////////////////////////////////End google examples

    public void startChat(View v){//Start next activity
        Intent intent = new Intent(this, ChatActivity.class);
        EditText editText = (EditText) findViewById(R.id.nameEditText);
        intent.putExtra(NICK_ID, editText.getText().toString());
        if(editText.getText().toString().equals("")){
            toastResId = R.string.warning;
            Toast.makeText(this, toastResId, Toast.LENGTH_SHORT).show();
        } else {
            startActivity(intent);
        }
    }
}

/*
    Shared Preferences are files on your filesystem that you read and edit using the
 "SharedPreferences" class. It acts like a key-value store, much like "Bundle", except
 that it is backed by persistent storage.
*/