package luca_teaching_appspot.homework3;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ergerodr on 2/17/16.
 */
public class Message {
    private String message;
    private String nickname;
    private String timestamp;
    private String messageID;
    private String conCat;

    public Message(String msg, String nname, String time, String msgID){
        message = msg;
        nickname = nname;
        timestamp = time;
        messageID = msgID;
        conCat = nname + ": " + msg;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessageID() {
        return messageID;
    }

    public void setMessageID(String messageID) {
        this.messageID = messageID;
    }

    public String getConCat(){ return conCat; }

    public void setConCat(String conCat){ this.conCat = conCat; }

    private static int lastMessageId = 0;

}
