package luca_teaching_appspot.homework3;

import java.math.BigInteger;
import java.security.SecureRandom;
/**
 * Created by ergerodr on 2/14/16.
 */

//"final" means this class can't be extended.
//Borrowed this code from professor luca's example.
public final class SecureRandomString {
    private SecureRandom random = new SecureRandom();

    public String nextString() {
        return new BigInteger(130, random).toString(32);
    }
}
