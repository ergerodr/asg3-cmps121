package luca_teaching_appspot.homework3.response;

//import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ergerodr on 2/14/16.
 *
 * Yea, I know I don't need this file. But at this point, I'm too lazy to change it.
 */
public class result {

    @SerializedName("result")
    @Expose
    private String result;

    /**
     *
     * @return
     * The result
     */
    public String getResult() {
        return result;
    }

    /**
     *
     * @param result
     * The result
     */
    public void setResult(String result) {
        this.result = result;
    }

}
