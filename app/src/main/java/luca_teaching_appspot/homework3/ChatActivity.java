package luca_teaching_appspot.homework3;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import luca_teaching_appspot.chatapp.R;
import luca_teaching_appspot.homework3.response.RefreshResponse;
import luca_teaching_appspot.homework3.response.ResultList;
import luca_teaching_appspot.homework3.response.result;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.GsonConverterFactory;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

/*
lat : latitude (float)
lng : longitude (float)
user_id : your user id (string)
nickname : nickname to talk to other users (string)
message : the message you are posting. (string)
message_id : a random string id you associate with each message. (string)
 */
public class ChatActivity extends AppCompatActivity {

    private ArrayList<Message> messages = new ArrayList<>();
    private RecyclerView rvMessages;
    private Button sendButton;
    private Button refreshButton;
    private messageAdapter adapter;
    private EditText msgField;
    private float lat;
    private float lng;
    private String uid;
    private String nickName;
    private int toastResId;

    private String LOG_TAG = "ChatActivity ";
    private LocationData mLocationData = LocationData.getLocationData();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        rvMessages = (RecyclerView) findViewById(R.id.messageView);
        // create adapter passing in the sample user data
        adapter = new messageAdapter(messages);
        // Attach adapter to the recyclerview to populate items
        rvMessages.setAdapter(adapter);
        // set layout manager to position the items
        rvMessages.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    public void onResume(){
        Intent intent = getIntent();
        nickName = intent.getStringExtra(MainActivity.NICK_ID);
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        lat = prefs.getFloat(MainActivity.LAT, 0);
        lng = prefs.getFloat(MainActivity.LNG, 0);
        uid = prefs.getString(MainActivity.USER_ID, "");
        sendButton = (Button) findViewById(R.id.sendButton);
        sendButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                handleMessages(lat, lng, uid, nickName);
            }
        });
        refreshButton = (Button) findViewById(R.id.refreshButton);
        refreshButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                retreiveMessages(lat, lng, uid);
            }
        });
        super.onResume();
    }
///////////////////////////CODE THAT "POST"S MESSAGES////////////////////////////////////
    private void handleMessages(float lat, float lng, String uid, String nickName){
        msgField = (EditText) findViewById(R.id.msgField);
        String message = msgField.getText().toString();
        SecureRandomString srs = new SecureRandomString();
        String msgid = srs.nextString();


        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();

        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient httpClient = new OkHttpClient.Builder()
                .addInterceptor(logging)
                .build();

        Retrofit retrofit = new Retrofit.Builder()//retrofit object does http call
                .baseUrl("https://luca-teaching.appspot.com/localmessages/default/")
                .addConverterFactory(GsonConverterFactory.create())//parses gson string
                .client(httpClient)
                .build();

        post_message service = retrofit.create(post_message.class);
        Call<result> call = service.createMessage(lat, lng, uid, nickName, message, msgid);
        Message msgItem = new Message(message, nickName, "Now", msgid);
        messages.add(msgItem);
        adapter.notifyItemInserted(messages.size() - 1);

        call.enqueue(new Callback<result>() {//does call in background
            @Override
            public void onResponse(Response<result> response) {
                String result = response.body().getResult();
                Log.i(LOG_TAG, result);
                if (result.equals("ok")) {
                    toastResId = R.string.sent;
                    Toast.makeText(ChatActivity.this, toastResId, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Throwable t) {

            }
        });
    }

    //Checks to make sure that no messages with the same msgID are posted. If a message has
    //the same messageID as another one, it is considered a duplicate and not added to the
    //RecyclerView
    public boolean checkMessage(Message msg, ArrayList<Message> list){
        for (int i = 0; i < list.size(); i++){
            if(list.get(i).getMessageID().equals(msg.getMessageID())){
                return true;
            }
        }
        return false;
    }

///////////////////////////CODE THAT "GET"S MESSAGES/////////////////////////////////////
    private void retreiveMessages(float lat, float lng, String uid){
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();

        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient httpClient = new OkHttpClient.Builder()
                .addInterceptor(logging)
                .build();

        Retrofit retrofit = new Retrofit.Builder()//retrofit object does http call
                .baseUrl("https://luca-teaching.appspot.com/localmessages/default/")
                .addConverterFactory(GsonConverterFactory.create())//parses gson string
                .client(httpClient)
                .build();

        get_messages service = retrofit.create(get_messages.class);
        Call<RefreshResponse> call = service.getMessage(lat, lng, uid);
        call.enqueue(new Callback<RefreshResponse>() {//does call in background
            @Override
            public void onResponse(Response<RefreshResponse> response) {
                String result = response.body().getResult();
                Log.i(LOG_TAG, result);
                if(result.equals("ok")){
                    toastResId = R.string.retreiving;
                    Toast.makeText(ChatActivity.this, toastResId, Toast.LENGTH_SHORT).show();
                    List<ResultList> resultList = response.body().getResultList();
                    int sizeBefore = messages.size();
                    for(int i = 0; i < resultList.size(); i++){
                        String msg = resultList.get(i).getMessage();
                        String nname = resultList.get(i).getNickname();
                        String stamp = resultList.get(i).getTimestamp();
                        String mid = resultList.get(i).getMessageId();
                        Message toInsert = new Message(msg, nname, stamp, mid);
                        if(!checkMessage(toInsert, messages)) {
                            messages.add(toInsert);
                            adapter.notifyItemInserted(messages.size() - 1);
                        }
                    }
                    if(sizeBefore == messages.size()){
                        int toastResId = R.string.msgFail;
                        Toast.makeText(ChatActivity.this, toastResId, Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Throwable t) {

            }
        });
    }

    public interface post_message {
        @POST("post_message")
        Call<result> createMessage(@Query("lat") float lat,
                                   @Query("lng") float lng,
                                   @Query("user_id") String userId,
                                   @Query("nickname") String name,
                                   @Query("message") String msg,
                                   @Query("message_id") String mId);
    }

    public interface get_messages {
        @GET("get_messages")
        Call<RefreshResponse> getMessage(@Query("lat") float lat,
                                         @Query("lng") float lng,
                                         @Query("user_id") String uid);
    }

    /****************************RECYCLERVIEW CODE BELOW******************************/

    // Provide a direct reference to each of the views within a data item
    // Used to cache the views within the item layout for fast access
    public class messageHolder extends RecyclerView.ViewHolder{
        // Your "ViewHolder" should contain a member variable
        // for any view that will be set as you render a row

        public TextView msgView;

        // We also create a constructor for "ViewHolder"that accepts the entire item row
        // and does the view lookups to find each subview
        public messageHolder(View itemView){
            // "super" Stores the itemView in a public final member variable that can be used
            // to access the context from any ViewHolder instance.
            super(itemView);
            msgView = (TextView) itemView.findViewById(R.id.messageBubble);
        }
    }

    // Create the basic adapter extending from RecyclerView.Adapter
    // Note that we specify the custom ViewHolder which gives us access to our views
    private class messageAdapter extends RecyclerView.Adapter <messageHolder> {

        //Member variable for messages to be displayed for our
        //messageAdapter class.
        private List<Message> mMessages;

        //Constructor for messageAdapter
        public messageAdapter(List<Message> messages){
            mMessages = messages;
        }


        @Override
        public messageHolder onCreateViewHolder(ViewGroup parent, int viewType){
            Context context = parent.getContext();
            LayoutInflater inflater = LayoutInflater.from(context);

            //Inflate layout
            View messageView = inflater.inflate(R.layout.list_item_message, parent, false);

            //Return a new holder instance
            messageHolder mHolder = new messageHolder(messageView);

            return mHolder;
        }


        //populates data into the item through "ViewHolder"
        @Override// (I.E., our "messageHolder" class)
        public void onBindViewHolder(messageHolder viewHolder, int position){
            //Get data model based on position
            Message message = mMessages.get(position);

            //Set item views based on the data model class "Messsage.java"
            TextView textView = viewHolder.msgView;
            textView.setText(message.getConCat());
        }


        @Override
        public int getItemCount(){
            return mMessages.size();
        }
    }
}
